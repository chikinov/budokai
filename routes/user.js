var crypto = require('crypto'); 
var express = require('express');
var router = express.Router();

router.post('/login', function(req, res)
{
    var db = req.db
    var users = db.get('users');
    var id = req.body.id;
    var uuid = req.body.uuid;
    users.findById(id, function(err, doc) {
        var json = new Object();
        if (err)
        {
            json.result = 0;
        }
		else if (doc)
        {
            var salt = doc.salt;
            var uuid_hash = crypto.pbkdf2Sync(uuid, salt, 256, 256).toString('hex');
            if (uuid_hash == doc.uuid_hash)
            {
                json.result = 1;
            }
            else
            {
                json.result = 3;
            }
        }
		else
        {
            json.result = 4;
        }
        res.json(json);
    });
});

router.post('/create', function(req, res)
{
    var db = req.db;
    var users = db.get('users');
    var uuid = req.body.uuid;
    var salt = crypto.randomBytes(256).toString('base64');
    var uuid_hash = crypto.pbkdf2Sync(uuid, salt, 256, 256).toString('hex');
    var id = users.id();
    users.insert({ _id: id, uuid_hash: uuid_hash, salt: salt });
    res.send(id);
});

module.exports = router;
