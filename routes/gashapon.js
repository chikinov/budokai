var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res)
{
	var id = Math.floor(Math.random() * 10000);
	res.send("You've got " + id.toString() + "/10000!");
});

module.exports = router;
